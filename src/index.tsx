import * as React from "react";
import * as ReactDOM from "react-dom";
import { SatetementsPage } from "./components/StetementsPage";
import { StatementStore } from "./model/StatementStore";

import dispatcher from "./dispatcher";
const store = new StatementStore();
dispatcher.register(a => console.log(`%c Action: ${a.action}`, 'color:green', a.payload));
dispatcher.register(store.handleAction.bind(store));

ReactDOM.render(
    <div className="container">
        <SatetementsPage statementStore={store}/>
    </div>,
    document.getElementById("example")
);

if(!store.loadFromLocalStoreage()) {
    loadTestdata();
}

function loadTestdata() {
            const testStatement = {
            date: 'Test Statement',
            accounts: [{
                        amount: 10000,
                        yearlyRate: 19,
                        name: "Depo",
                        currency: "uah",
                        type: "depo"
                    },{
                        amount: 1000,
                        yearlyRate: 23,
                        name: "Инвест",
                        currency: "uah",
                        type: "invest"
                    }, {
                        amount: 1000,
                        yearlyRate: 0,
                        name: "Cell",
                        currency: "usd",
                        type: "cash"
                    }, {
                        amount: 1000,
                        yearlyRate: 0,
                        name: "Cell",
                        currency: "gbp",
                        type: "cash"
                    }]
        };

        store.importData([testStatement]);
}