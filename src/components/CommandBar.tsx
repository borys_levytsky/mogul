import { addStatements } from '../statementActions';
import * as React from "react";
import * as actions from "../statementActions";
import { Statement, StatementData } from '../model/Statement';
import { ExtractForm } from './ExtractForm';
import * as util from '../utility'; 

export class CommandBarProps {
    statements: Statement[]
}

export class CommandBar extends React.Component<CommandBarProps, CommandBarProps> {    
    render() {
        return  <div style={{padding:'20px'}}>
                    <div className="btn-group" role="group">
                        <label className="btn btn-primary btn-file">
                            Import Data <input type="file" id="files" style={{ display: "none"}} name="file" onChange={this.importData.bind(this)}/>
                        </label>
                        <a download="satementdata.json" className="btn btn-warning" href={'data:application/json;charset=utf-8,'+ encodeURIComponent(JSON.stringify(this.props.statements.map(m => m.getData())))}>Export Data</a>
                        <button className="btn btn-success" onClick={e => actions.toggleExtractForm()}>Extract from PrivatBank</button>
                        <label className="btn btn-primary btn-file">
                                    Add statement from file <input type="file" style={{ display: "none"}} name="file" onChange={this.addStatements.bind(this)}/>
                        </label>
                    </div>
                    
                    <ExtractForm />                   
                </div>;
    }

    importData(evt: any) {
        const input = evt.target as HTMLInputElement;
        var file = input.files[0];
        console.log(file);
        util.readJsonFile<StatementData[]>(file, data => actions.importData(data));
    }

    addStatements(e : Event) {
        const input : HTMLInputElement = e.target as HTMLInputElement;
        const file = input.files[0];
        util.readJsonFile<StatementData[]>(file, data => actions.addStatements(data));
    }    
}