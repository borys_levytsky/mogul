import { Statement } from '../model/Statement';
import * as React from 'react';
import { AccountsGrid } from './AccountGrid/AccountsGrid';
import { Amount } from '../model/Currency';

export interface StatementBlockProps {
    statement: Statement,
    prevStatement: Statement
}

export class StatementBlock extends React.Component<StatementBlockProps, {}> {
    render () {
        const {statement, prevStatement} = this.props;
        const totals = statement.totals;
        console.log(`%c Render statement block ${statement.date}`, 'color:green; font-weight: bold')
        return <div className="col-md5">
                    <div>
                        <h2 style={{display: "inline"}}>{statement.date}</h2>
                        <a href="javascript:void(0)"><i className='glyphicon glyphicon-remove'></i>Remove</a>
                    </div>
                    <div>
                        <a download="statement.json" href={'data:application/json;charset=utf-8,'+ encodeURIComponent(JSON.stringify(statement, null, '\t'))}>Export Statement</a>
                    </div>
                    <AccountsGrid statement={statement} prevStatement={prevStatement} />
                </div>
    }
}