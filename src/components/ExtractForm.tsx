import * as React from "react";
import * as util from "../utility";
import * as actions from "../statementActions";
import { StatementData } from "../model/Statement";
import dispatcher from "../dispatcher";

export interface ExtractFormState {
    scriptExtension: string,
    visible: boolean;
}

export class ExtractForm extends React.Component<{}, ExtractFormState> {
    constructor () {
        super();
        this.state = {
            scriptExtension: localStorage.getItem('scriptExtension') || '',
            visible: false
        }
    }
    componentWillMount(state: ExtractFormState) {
        dispatcher.register(a => {
            if(a.action == 'TOGGLE_EXTRACT_FORM') {
                this.newState({ visible: !this.state.visible });
            }
        });
    }
    render() {
            return <div className="panel panel-default" style={this.getStyle()}>
                        <div className="panel-heading">Extract Statements</div>
                        <div className="panel-body">
                        <div className="row">
                            <div className="cold-md-5">
                                <p>Execute this function on PrivatBank page</p>                        
                                <textarea cols={100} rows={10} value={getExportFunc(this.state.scriptExtension)}></textarea>
                            </div>                            
                                <div className="col-md-5">
                                    <textarea id="txtExtend" rows={5} cols={100}
                                        onChange={this.handleExtensionChange.bind(this)} 
                                        defaultValue={this.state.scriptExtension}></textarea>
                                </div>
                            </div>
                        </div>
                   </div>;
    }
    getStyle() {
        return { display: this.state.visible ? '' : 'none' }
    }

    handleExtensionChange(e : Event) {
            var t = e.target as HTMLTextAreaElement;
            localStorage.setItem('scriptExtension', t.value);
            this.newState({ scriptExtension: t.value });
    }

    private newState(newProps : any ) {
        this.setState(Object.assign({}, this.state, newProps));
    }

    private toggleForm() {
        this.newState({ visible: !this.state.visible });
    }
}

function getExportFunc(ext: string) : string {
            return  `(function() {
                                var rows = document.getElementById('depositsTable').getElementsByClassName('td');
                                if(rows.length == 0) return;

                                console.log('rows length:' + rows.length)
                                var depos = [],  amt, depo, date;
                                for(var i=0; i<rows.length;i++) {
                                    var row = rows[i];
                                    date = row.getElementsByClassName('column6')[0].textContent.substr(10) 
                                    amt = row.getElementsByClassName('column14')[0].textContent;
                                    depo =
                                    {
                                        type: "depo",
                                        name: row.getElementsByClassName('column20')[0].textContent,
                                        yearlyRate: parseFloat(row.getElementsByClassName('column12')[0].textContent.replace('%')),
                                        amount: parseFloat(amt.replace(/[^\\d\\.]/g, '')),
                                        currency: /долл/.test(amt) ? "usd" : "uah",
                                        date: date
                                    };

                                    depos.push(depo);
                                    var statement = {
                                        accounts: depos,
                                        date: new Date().toString()
                                    }
                                }

                                ${ext}

                                var json = JSON.stringify([statement]);
                                console.log(json);
                                var link = document.createElement('a');
                                link.href = 'data:application/json;charset=utf-8,' + json;
                                var date = new Date();
                                link.setAttribute('download', 'statement-' + date.getDate() + '.' + date.getMonth() +  '.json')
                                link.click();
                            })();`;
}

