import * as React from "react";
import { AccountsGrid, AccountsGridProps } from "./AccountGrid/AccountsGrid";
import { StatementBlock } from './StatementBlock';import { Statement } from '../model/Statement';
import { StatementStore } from '../model/StatementStore'; 
import { CommandBar } from './CommandBar';
import * as actions from "../statementActions";


interface StatementState {
    statements: Statement[]
}

export interface StatementsPageProps {
    statementStore: StatementStore
}

export class SatetementsPage extends React.Component<StatementsPageProps, StatementState> {
    componentWillMount() {
        this.props.statementStore.attach(() => this.updateStatements())
        this.updateStatements();
    }

    render() {
        var { statements } = this.state;
        var blocks = statements.map((s,i) => <StatementBlock statement={s} key={i} prevStatement={this.findPrevStatement(i)} />);        
        return <div>
                    <div>
                        <CommandBar statements={this.state.statements || []}/>
                    </div>
                
                    <div className="col-md5">
                        {blocks}
                    </div>
                </div>
    } 

    updateStatements() {
         this.setState({
            statements: this.props.statementStore.getStatements()
        });
    }

    findPrevStatement (index: number) {
        if(index == this.state.statements.length) {
            return  null;
        }

        return this.state.statements[index+1];
    }
}