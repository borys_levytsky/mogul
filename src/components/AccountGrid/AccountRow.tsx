import * as React from 'react';
import * as _ from 'lodash';
import * as moment from 'moment';

import { Account } from '../../model/Account';
import { Func } from '../../model/Func';
import { Amount } from '../../model/Currency';


export interface AccountRowProps {
    account: Account,
    prevAccount: Account
}

export class AccountRow extends React.Component<AccountRowProps, {}> {
     render() {
        const { account } = this.props;
        const { currency } = account.balance;
        return <tr>
                    <td>{account.name}</td>
                    <td>{this.getDate(account.date)}</td>
                    <td>{this.getBalance(a => a.balance)}</td>                    
                    <td>{account.yearlyRate > 0 ? account.yearlyRate : ''}</td>
                    <td>{account.yearlyRate > 0 ? account.monthlyRate.toFixed(2) : ''}</td>
                    <td>{account.yearlyRate > 0 ? this.getBalance(a => a.getMonthlyIncome()) : ''}</td>
                </tr>;
    }

    getBalance(getBalance: Func<Account, Amount>) {
        const { account, prevAccount } = this.props;
        console.log(this.props);        
        const balance = getBalance(account);
        const prevBalance = this.props.prevAccount ? getBalance(prevAccount) : balance;

        const amt = <span className={balance.currency}>{balance.toString()}</span>
        var alt = balance.convertTo(balance.currency == "UAH" ? "USD" : "UAH");
        return <div>
                    <div>
                            {amt}{this.getDiff(balance.value, prevBalance.value)} 
                    </div>
                    <div>
                            <span className="small-amt">{alt.toString()}</span>
                    </div>
                </div>;
    }    

    getDate(date: moment.Moment) {
        if(!date) {
            return null;
        }

        return <div>
                    {date.format(Account.dateFormat)}
                    <div className="small-amt">
                        {moment().to(date)}                       
                    </div>
                </div>;
    }

    
    getDiff(balance: number, prevBalance: number) {
        const diff = balance - prevBalance;
        if(diff == 0) {
            console.log('diff is zero');
            return null;
        }

        var pref = '';
        var className = "balance-diff";
        if(diff > 0) {
            pref = '+';
            className += " positive";
        }
        if(diff < 0) {
            className += " negative";
        }
        return <span className={className}>{pref}{diff.toFixed(2)}</span>;
    }

    getBalanceDifference() {
        const { account, prevAccount } = this.props;
        if(!prevAccount) {
            console.log('no prev account');
            return 0;
        }

        return account.balance.value - prevAccount.balance.value;
    }
}
