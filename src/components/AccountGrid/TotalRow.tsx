import * as React from 'react';
import { Statement } from '../../model/Statement';
import { Amount } from '../../model/Currency';

export class TotalRow extends React.Component<{ statement: Statement, prevStatement: Statement}, {}> {
    render() {
        return <tr>
            <td><b>Total</b></td>
            <td></td>
            <td>
                {this.getTotalBalance()}
            </td>                    
            <td></td>
            <td></td>
            <td>
                {this.getTotalIncome()}
            </td>
        </tr>
    }

    getTotalBalance() : JSX.Element[] {
        const { statement, prevStatement } = this.props;
        return statement.totals.map((t, i) => <div key={i} className={t.balance.currency}>{t.balance.toString()}</div>);
    }

    getTotalIncome() {
        const { statement, prevStatement } = this.props;
        return statement.totals.map((t, i) => <div key={i} className={t.monthlyIncome.currency}>{t.monthlyIncome.toString()}</div>);
    }

    getDiff(amt: Amount, prevAmt : Amount) {
        if(amt.value == prevAmt.value) {
            return <span>+0</span>;
        }

        if(amt.value < prevAmt.value) {
            return <span>-{(prevAmt.value - amt.value).toFixed(2)}</span>;
        }

        return <span>+{(amt.value - prevAmt.value).toFixed(2)}</span>;
    }
}