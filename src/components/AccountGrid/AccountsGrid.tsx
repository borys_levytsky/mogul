import * as React from "react";
import * as _ from "lodash";
import {Account} from "../../model/Account";
import {Statement} from "../../model/Statement";
import {Amount} from "../../model/Currency";
import {Func} from "../../model/Func";
import {AccountRow} from './AccountRow';
import {TotalRow} from './TotalRow';

import * as moment from "moment";

export interface AccountsGridProps {
    statement: Statement,
    prevStatement: Statement
}

export class AccountsGrid extends React.Component<AccountsGridProps, {}> {
    render() {
        const { accounts } = this.props.statement;
        const rows = _.chain(accounts)
                .orderBy(a => this.getSortValue(a))
                .map((a, i) => <AccountRow key={i} account={a} prevAccount={this.getPrevAccount(a)} />)
                .value();

        const totalIncome = accounts.map(a => a.getMonthlyIncome()).reduce((p, c) => p.addWithConversion(c), Amount.zero('UAh'));
        rows.push(<TotalRow key={accounts.length} statement={this.props.statement} prevStatement={this.props.prevStatement} />);

        return <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Balance</th>
                        <th>Yearly Rate %</th>
                        <th>Monthly Rate %</th>
                        <th>Monthly Income</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>    
              </table>;
    }
    getSortValue(account : Account) {
        if(account.date == null) {
            return 100000001;
        }

        if(account.date.toDate() < new Date()) {
            // Date in the past
            return 10000000;
        }
        
        return account.date.diff(moment(), 'day');
    }

    getPrevAccount(acc: Account) : Account {
        if(this.props.prevStatement == null) {         
            return null;
        }
        
        return  this.props.prevStatement.accounts.filter(a => acc.isTheSame(a))[0] || null;
    }
}
