import { AccountData, Account } from './Account';
import { Amount } from './Currency';

export interface StatementData {
    accounts: AccountData[],
    date: string,
}


interface CurrencyTotal {
    balance: Amount,
    monthlyIncome: Amount,
    currency: string
}

export class Statement {
    accounts: Account[];
    totals: CurrencyTotal[];
    date: string;
    constructor(data: StatementData) {
        this.accounts = data.accounts.map((acc,i) => new Account(acc));
        this.date = data.date;
        this.calcTotals();
    }
    getTotal(currency: string) {
        const found = this.totals.filter(t => t.currency == currency)[0];
        if (found == null) {
            throw new Error(`Cannot find total for ${currency} currency`)
        }
        return found;
    }
    calcTotals() : void {
        const currencies = ['UAH', 'USD', 'EUR'];            
        this.totals = currencies.map(c => this.calcTotal(c));          
    }
    calcTotal(currency : string) : CurrencyTotal {
        const sum = new Amount(0, currency);
        
        const balance = this.accounts
            .map((acc,i) => acc.balance)
            .reduce((c, p) => c.addWithConversion(p), sum);

        const monthlyIncome = this.accounts
            .map((acc,i) => acc.getMonthlyIncome())
            .reduce((c, p) => c.addWithConversion(p), sum);

        return { balance, monthlyIncome, currency };
    }

    getData() : StatementData {
        return {
            date: this.date,
            accounts: this.accounts.map(a => a.getData())
        }   
    }
}