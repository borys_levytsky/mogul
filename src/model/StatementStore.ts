import { Statement, StatementData } from "./Statement";
import { addStatements } from "./../statementActions";
import { Action } from "../dispatcher";
import { SyncEvent } from "ts-events";

const satetementDataKey = 'satetementData';

export interface StatementList {
    statements: Statement[]
}

export class StatementStore extends SyncEvent<string> {
    data: StatementData[];
    constructor() {
        super();
        this.data = [];
    }

    getStatements() {
        return this.data.map(d => new Statement(d));
    }

    importData(data: StatementData[]) {
        this.data = data;
        console.log('importData: data imported', this.data);
        this.triggerChanged();
    }

    hasLocalStorageData() {
        return window.localStorage.getItem(satetementDataKey) != null;
    }

    loadFromLocalStoreage () : Boolean {
        const json = window.localStorage.getItem(satetementDataKey);
        if (json) {
            try
            {
                const data = JSON.parse(json);
                this.importData(data);
                return true;
            }
            catch(e) {
                console.error(`Failed to load statement data from local storage: ${e.message}`)
                console.log(json);
                window.localStorage.removeItem(satetementDataKey);
                return false;
            }
        }
    }

    handleAction (action : Action) {
        switch(action.action) {
            case "IMPORT_DATA": {
                this.importData(action.payload);
                break;
            }
            case "ADD_STATEMENTS": {
                this.addStatements(action.payload);
                break;
            }
        }
    }

    private addStatements(statements : StatementData[]) {
       console.log('addStatements', statements, new Error().stack);
       this.data.unshift(...statements);
       console.log('data after', this.data)
       this.triggerChanged();
    }

    private triggerChanged() {
         this.saveToLocalStorage();
         this.post('changed');
    }

    private saveToLocalStorage() {
        console.log(this.data);
        window.localStorage.setItem(satetementDataKey, JSON.stringify(this.data));
    }
}