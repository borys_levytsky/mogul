import {Amount} from "./Currency";
import * as moment from "moment";
import * as _ from "lodash";

export interface AccountData {
    name: string; 
    amount: number;
    type: string;
    yearlyRate: number;
    currency: string,
    date?: string
}

export class Account {
    static dateFormat: string = 'DD.MM.YYYY';
    name: string;
    date?: moment.Moment; 
    balance: Amount;
    type: string;
    yearlyRate: number;
    monthlyRate: number;
    constructor(data: AccountData) {
        this.name = data.name;
        this.balance = new Amount(data.amount, data.currency.toUpperCase());
        this.type = data.type;
        this.yearlyRate = data.yearlyRate;
        this.monthlyRate = this.getMonthlyRate();
        this.date = data.date ? moment(data.date, Account.dateFormat) : null;
    }
    getMonthlyRate () {
        return this.yearlyRate / 12;
    }

    getMonthlyIncome () : Amount {
        if(this.monthlyRate == 0) {
            return Amount.zero(this.balance.currency);
        }
        var number = this.balance.value * (this.monthlyRate / 100);
        return new Amount(number, this.balance.currency);
    }
   
    getData() : AccountData {
        return {
            name: this.name,
            type: this.type,
            yearlyRate: this.yearlyRate,
            amount: this.balance.value,
            currency: this.balance.currency,
            date: this.date ? this.date.format(Account.dateFormat) : null,
        };
    }

    isTheSame(account: Account) : boolean {
        return this.name == account.name && this.balance.currency == this.balance.currency;    
    }

    toString() {
        return `${this.name} ${this.balance}`;
    }
}