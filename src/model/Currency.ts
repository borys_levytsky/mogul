import { Account } from './Account';
import { Statement } from './Statement';
export class Amount {
    constructor(public value: number, public currency: string) {
    }
    
    convertTo(newCurrency: string) : Amount {    
        const rate = conversionRates.findRateFor(this.currency, newCurrency);
        var amt = new Amount(this.value * rate, newCurrency);
        return amt;
    }
    
    addWithConversion(anotherAmount: Amount) : Amount {
        const converted = anotherAmount.convertTo(this.currency);
        return new Amount(this.value + converted.value, this.currency);
    }

    toString () {
        return this.format(this.value) + " " + this.currency;
    }

    equalsTo(amount : Amount) : boolean {
        return this.value == amount.value && this.currency == amount.currency;
    }

    private format (amount: number){
       return parseFloat(amount.toFixed(2)).toLocaleString();
    }

    static zero(currency: string) {
        return new Amount(0, currency);
    }
}

const conversionRates = {
    'UAH': {
        'USD': 0.039239,
        'UAH': 1,
        'EUR': 0.0352171962,
        'GBP': 0.0313448097
    },
    'USD': {
            'USD': 1,
            'UAH': 25.4848493,
            'EUR': 1.1142,
            'GBP': 1.25185
    },
    'EUR': {
            'UAH': 28.395219,
            'USD': 0.897504936,
            'GBP': 1.12354155
        },
    'GBP': {
        'USD': 0.79881775,
        'UAH': 31.9032085,
        'EUR': 0.890042737
    },
    findRateFor(from: string, to: string) : number {
        const fr = this.findRate(from);
        return fr[to];
    },
    findRate (currency: string) {
        const found = this[currency.toUpperCase()];
        if(!found) {
            throw new Error(`Currency ${currency} is not found.`)
        }
        return found;
    }
 }











