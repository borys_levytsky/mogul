import { Dispatcher } from "flux";
export interface Action {
    action: string,
    payload: any
}
export default new Dispatcher<Action>();