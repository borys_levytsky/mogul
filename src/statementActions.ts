import dispatcher from "./dispatcher";
import {StatementData} from "./model/Statement";

export const importData = function(statements: StatementData[]) {
    dispatcher.dispatch({
        action: "IMPORT_DATA",
        payload: statements
    });
}  

export const addStatements = function(statements: StatementData[]) {
    dispatcher.dispatch({
        action: "ADD_STATEMENTS",
        payload: statements
    });
}

export const toggleExtractForm = function() {
dispatcher.dispatch({
        action: "TOGGLE_EXTRACT_FORM",
        payload: null
    });
}
