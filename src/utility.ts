export const readJsonFile = function<T>(file: File, handle: (c: T) => void) {
    var reader = new FileReader();
        reader.onload = f => handle(JSON.parse(reader.result));
        reader.readAsText(file);
}